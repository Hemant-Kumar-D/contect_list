package com.example.contects_app.RecyiclerAdapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.contects_app.Entity.UsercontectInfo
import com.example.contects_app.R
import com.example.contects_app.databinding.SavenumberfoemateBinding
import java.util.Base64


class Adapter(private var listofditail:List<UsercontectInfo>): RecyclerView.Adapter<Adapter.myviewholder>(){
    inner  class myviewholder(var binding:SavenumberfoemateBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myviewholder {
  var view:View=LayoutInflater.from(parent.context).inflate(R.layout.savenumberfoemate,parent,false)
        var binding:SavenumberfoemateBinding=DataBindingUtil.bind(view!!)!!
        return  myviewholder(binding)
    }

    override fun getItemCount(): Int {
        return listofditail.size
    }

    override fun onBindViewHolder(holder: myviewholder, position: Int) {
     val user=listofditail[position]
        holder.binding.apply {
            this.userName.text=user.NAME
            this.title.text=user.TITLE
            this.profilesetImage.setImageBitmap(convertertobitmap(user.IMAGE))

        }
    }
    fun convertertobitmap( base64Image:String): Bitmap?{

        val decodedString: ByteArray = android.util.Base64.decode(base64Image,android.util.Base64.NO_WRAP)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        return decodedByte
    }
}