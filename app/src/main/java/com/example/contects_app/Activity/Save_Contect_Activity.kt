package com.example.contects_app.Activity

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.contects_app.Entity.UsercontectInfo
import com.example.contects_app.R
import com.example.contects_app.Repogetry.contectdetailrepogetry
import com.example.contects_app.Room_Db.ContectDatabase
import com.example.contects_app.contectfactory.contectfactory
import com.example.contects_app.databinding.ActivitySaveContectBinding
import com.example.contects_app.viewmodel.Contectviewmodel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream

class Save_Contect_Activity : AppCompatActivity() {
    private lateinit var binding:ActivitySaveContectBinding
    lateinit var factory:contectfactory
    lateinit var viewmdoel:Contectviewmodel
    var base64Image:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this, R.layout.activity_save_contect)
        factory= contectfactory(contectdetailrepogetry(ContectDatabase.Contactatabse(this).contectdao()))
        viewmdoel=ViewModelProvider(this,factory)[Contectviewmodel::class.java]
    }
    fun PhotoClick(view:View){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 1002)
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 1001)
        }
    }

     fun Numbersaved(view: View){
         CoroutineScope(Dispatchers.IO).launch {
             if(base64Image.isNotEmpty()) {
                 viewmdoel.insertcont(
                     UsercontectInfo(
                         0,
                         binding.editName.editText?.text.toString(),
                         binding.editEmail.editText?.text.toString(),
                         binding.editMobile.editText?.text.toString(),
                         binding.editCompany.editText.toString(),
                         binding.editTitle.editText?.text.toString(),
                         base64Image

                         )
                 )
                 withContext(Dispatchers.Main){
                     Toast.makeText(this@Save_Contect_Activity, "Inserted Succefully.", Toast.LENGTH_SHORT).show()
                     refresh(view)
                     finish()
                 }

             }
             else {
                 withContext(Dispatchers.Main) {
                     Toast.makeText(
                         this@Save_Contect_Activity,
                         "Something went wrong.",
                         Toast.LENGTH_SHORT
                     ).show()
                 }
             }

         }

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1001) {
            if (grantResults.isNotEmpty() && permissions[0].equals(PackageManager.PERMISSION_GRANTED)) {

            } else {
                Toast.makeText(this, "Please give permission", Toast.LENGTH_SHORT).show()
            }

        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1002 && resultCode == RESULT_OK){
            var bitmp: Bitmap = data?.extras?.get("data") as Bitmap
            binding.profileImage.setImageBitmap(bitmp)
            base64Image = convertBitmapToBase64(bitmp)

            Log.d("TAG", "onActivityResult: $base64Image")

        }else{

        }
    }
    private fun convertBitmapToBase64(bitmap: Bitmap): String{
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }
    suspend fun refresh(view:View){
        startActivity(Intent(this,MainActivity::class.java))
    }



}