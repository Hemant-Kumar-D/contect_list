package com.example.contects_app.Activity

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.contects_app.Entity.UsercontectInfo
import com.example.contects_app.R
import com.example.contects_app.RecyiclerAdapter.Adapter
import com.example.contects_app.Repogetry.contectdetailrepogetry
import com.example.contects_app.Room_Db.ContectDatabase
import com.example.contects_app.contectfactory.contectfactory
import com.example.contects_app.databinding.ActivityMainBinding
import com.example.contects_app.viewmodel.Contectviewmodel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var factory: contectfactory
    lateinit var viewmdoel: Contectviewmodel
    private lateinit var myadpter:Adapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        factory =
            contectfactory(contectdetailrepogetry(ContectDatabase.Contactatabse(this).contectdao()))
        viewmdoel = ViewModelProvider(this, factory)[Contectviewmodel::class.java]
        viewmdoel.getalldata.observe(this, Observer {
            binding.reccyclerview.layoutManager = LinearLayoutManager(this)
            myadpter = Adapter(it)
            binding.reccyclerview.adapter = myadpter
        })

    }



    fun SaveContect(view:View){
        startActivity(Intent(this, Save_Contect_Activity::class.java))
      }




}