package com.example.contects_app.Entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
 data class UsercontectInfo(
     @PrimaryKey(autoGenerate = true) var SRNU:Int,
     @ColumnInfo("NAME") var NAME:String,
     @ColumnInfo("Mo.Number") var MOBILE:String,
     @ColumnInfo("COMPANY NAME") var COMPANY:String,
     @ColumnInfo("TITLE") var TITLE:String,
     @ColumnInfo("E-mail") var EMAIL:String,
     @ColumnInfo("image") var IMAGE:String,
)
