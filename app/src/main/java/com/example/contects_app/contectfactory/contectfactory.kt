package com.example.contects_app.contectfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.contects_app.Repogetry.contectdetailrepogetry
import com.example.contects_app.viewmodel.Contectviewmodel

class contectfactory(private val contectrepo:contectdetailrepogetry):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(Contectviewmodel::class.java)){
            return Contectviewmodel(contectrepo) as T
        }
        throw IllegalArgumentException("Unknown class")
    }


}