package com.example.contects_app.Room_Db

import android.content.Context
import androidx.room.Database
import androidx.room.Room

import androidx.room.RoomDatabase
import com.example.contects_app.Entity.UsercontectInfo



@Database(entities = [UsercontectInfo::class], version = 1)
abstract class ContectDatabase():RoomDatabase() {
    abstract fun contectdao():ContectDao
    companion object {
        @Volatile
        var INTENCE: ContectDatabase? = null
        fun Contactatabse(context: Context): ContectDatabase {
            var instence=INTENCE
            if (INTENCE == null) {
                synchronized(this) {
                    instence =
                        Room.databaseBuilder(context, ContectDatabase::class.java, "CONTECT")
                            .build()
                     INTENCE=instence
                }

            }
            return INTENCE!!
        }
    }


}