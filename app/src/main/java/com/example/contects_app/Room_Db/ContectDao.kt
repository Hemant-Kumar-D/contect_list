package com.example.contects_app.Room_Db

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.contects_app.Entity.UsercontectInfo

@Dao
interface ContectDao{
    @Insert
    suspend fun insetContact( vararg usercontectInfo: UsercontectInfo)
    @Query("SELECT*FROM Usercontectinfo")
    fun getalldata(): LiveData<List<UsercontectInfo>>


}